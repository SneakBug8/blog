const fs = require('fs');
const readline = require('readline');
const request = require('request');
const JSONStream = require('JSONStream')


const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var lasturl = null

function askurl() {
    return new Promise((resolve, reject) => {
        rl.question('Wordpress site url? ', (answer) => {
            lasturl = answer
            rl.close();
            resolve(true)
        })
    })
}

askurl()
    /*
    .then(() => {
        var start = new Date();
        var oldconfig = {}

        request(lasturl + "/wp-json/wp/v2/settings", function (error, response, body) {
            oldconfig = JSON.parse(body);
            console.log('Got settings in ' + (new Date() - start) + ' ms.');
          });

        var config = {
            title: oldconfig.title
        }
        
        fs.writeFile('config.json', JSON.stringify(config), (err) => {
            console.log('Successfully exported settings in ' + (new Date() - start) + ' ms.');    
        })
    })*/
    .then(() => {
        var start = new Date();

        var wpposts = []

        function getposts(page) {
            request(lasturl + '/wp-json/wp/v2/posts?posts_per_page=10&page=' + page, function (error, response, body) {
                var addposts = JSON.parse(body);
                console.log(page + " | " + addposts.toString()); 
                
                if (Array.isArray(addposts) && page < 100) {
                    wpposts = wpposts.concat(addposts);
                    return getposts(page+1);                    
                } else {
                    console.log('Got posts in ' + (new Date() - start) + ' ms.');                    
                    processposts(wpposts);
                    return false;
                }
            });
        }

        getposts(1)

        function processposts(wpposts) {
            posts = []

            posts = wpposts.map((oldpost, ind) => {
                console.log(oldpost, { depth: null });
                return post = {
                    id: ind,
                    title: oldpost.title.rendered,
                    link: oldpost.link.replace(lasturl, ''),
                    date: oldpost.date,
                    author: oldpost.author,
                    content: oldpost.content.rendered,
                    excerpt: oldpost.excerpt.rendered
                }
            });

            fs.writeFile('posts.json', JSON.stringify(posts), (err) => {
                console.log('Successfully exported ' + posts.length + ' posts in ' + (new Date() - start) + ' ms.')
            });
        }
    });