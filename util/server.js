/*
Used to launch site on Node.js hosting, like openode
*/

const serve = require('serve')

const server = serve("build", {
    port: 80
  });