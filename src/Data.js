// import * as firebase from "firebase";

export const config = {
  title: "SneakBug8's Blog",
  separator: " | ",
  firebase: {
    apiKey: "AIzaSyA0aWT1y7Xr_F1lL-5K5dJgTAT_Ba3yFDs ",
    authDomain: "sneakbug8-blog.firebaseapp.com",
    databaseURL: "https://sneakbug8-blog.firebaseio.com/",
    // storageBucket: "<BUCKET>.appspot.com",
  }
};


export class Data {
  posts = []

  init() {
    return new Promise((resolve, reject) => {
      window.config = config;
      /* this.loadFirebase()
      .then(() => this.loadPosts())
      .then(() => resolve(true)) */
      this.loadPosts()
      .then(() => console.log(window.posts))
      .then(() => resolve(true))
    }) 
  }


  /* loadFirebase() {
    return new Promise((resolve, reject) => {
      firebase.initializeApp(config.firebase);
      window.firebase = firebase;
      resolve(true)
    })
  } */

  loadPosts() {
    return new Promise((resolve, reject) => {
      let staticposts = require('./posts.json')
      /* this.posts = this.posts.concat(staticposts);
      window.firebase.database().ref('/posts').once('value')
      .then((posts) => window.posts = this.posts.concat(posts.val())) */
      window.posts = staticposts;
      resolve(true);
    })
  }
}