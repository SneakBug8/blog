import React, {Component} from 'react';

class Menu extends Component {
    constructor(props) {
        super()
        this.props = props;
    }
    render() {
        let menu = this.props.children.map((child, index) => 
            <li className="nav-item" key={index}>
                {child}
            </li>
        )
        return (
            <nav className="nav">
                <ul className="nav-list">
                    {menu}
                </ul>
            </nav>
        );
    }
}

export default Menu;