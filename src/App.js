//@flow

import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Link, Switch, NavLink} from 'react-router-dom'
import './css/App.css';
import {config, Data} from './Data';

import Error404 from './views/404';
import Post from './views/Post.js';
import PostList from './views/PostList.js';
import AboutMe from './views/AboutMe';
import Archive from './views/Archive';
import Menu from './components/Menu';
import Footer from './components/Footer';

/* Main application screen */
class App extends Component {
		constructor(props) {
				super()
				let data = new Data();
				data.init()
					.then((res) => {
							this.setState({rerender: true})
							this.posts = window.posts;
					})
		}
		/*<Route path='/:url' component={Post}/>*/

		render() {
				let routing = (
						<Switch>
								<Route exact path='/' component={PostList}/>
								<Route exact path='/about' component={AboutMe}/>								
								<Route exact path='/archive' component={Archive}/>
								{(this.posts !== undefined) ? this.posts.map((post, ind) => {
									return <Route exact path={post.link} component={Post}/>;						
								}) : null}
								<Route path='*' component={Error404} status={404}/>
						</Switch>
				)
				return (
						<Router>
								<div>
										<div className="sidebar pure-u-1 pure-u-md-1-4">
												<div className="header">
														<Link
																to={{
																pathname: "/",
																search: "",
																state: {}
														}}>
																<h1 className="brand-title">SneakBug8's Blog</h1>
														</Link>
														<h2 className="brand-tagline">Blog about web and game development</h2>

														<Menu>
															<NavLink className="pure-button" exact to="/about">About Me</NavLink>
															<NavLink className="pure-button" exact to="/archive">Archive</NavLink>
														</Menu>																			
												</div>
										</div>

										<div className="content pure-u-1 pure-u-md-3-4">
												<div>
														{routing}
														<Footer/>
												</div>
										</div>
								</div>
						</Router>
				);
		}
}

export default App;
