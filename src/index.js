import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Error404 from './views/404';
import App from './App';
import Admin from './admin/Admin';
import registerServiceWorker from './registerServiceWorker';

class Index extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path='/admin' component={Admin}/>
                    <Route exact path='/error' component={Error404}/>                                                       
                    <Route path='/*' component={App}/>
                    <Route path='*' component={Error404}/>                 
                </Switch>
            </Router>)
    }
}

ReactDOM.render(<Index/>, document.getElementById('layout'));
registerServiceWorker();
