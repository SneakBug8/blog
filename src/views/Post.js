import React, { Component } from 'react';
import Loader from './../components/Loader.js';
import {Redirect} from 'react-router-dom'

/* Single Post page*/
class Post extends Component {
    post = {}

    constructor (props) {
        super()

        this.posts = window.posts;

        for (var i of this.posts) {
            if (props.match.url === i.link) {
                this.post = i;
                this.state = {rerender: true}
                break;
            }
        };

        document.title = window.config.title + window.config.separator + this.post.title;
    }
    render() {
        return (
            <section className="post">
            <header className="post-header">
                <h2 className="post-title">{this.post.title} </h2>

                <p className="post-meta">
                    Posted at {this.post.date}
                </p>
            </header>
            <div className="post-description">
                <p dangerouslySetInnerHTML={{__html: this.post.content}}/>
            </div>
        </section>);
    }
}

export default Post;