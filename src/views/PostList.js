import React, {Component, PureComponent} from 'react';
import {Link} from 'react-router-dom';
import autoBind from 'react-autobind';
import Loader from './../components/Loader.js';
const queryString = require('query-string');


/* Blog post page */
class PostList extends PureComponent {
		constructor(props) {
				super()
				autoBind(this);				
				this.props = props;

				this.state = {
					page: 1
				}

				this.extractPage(props.location.search);
		}

		extractPage(search) {
			let query = queryString.parse(search);
			let page = (query.page) ? parseInt(query.page, 10) : 1;
			this.setPage(page);
		}

		nextPage(event) {
				this.changePage(1);
				event.preventDefault();
		}

		prevPage(event) {
				this.changePage(-1);
				event.preventDefault();
		}

		changePage(pagechange) {
			let page = this.state.page + pagechange;
			this.setPage(page);
		}

		setPage(pageind) {
			if (pageind === this.state.page) {
				return;
			}

			this.props.history.push('?' + queryString.stringify({
					page: pageind
			}));
			this.setState({
				page: pageind
			})
		}

		componentWillReceiveProps(nextProps) {
				if (this.props.location.search !== nextProps.location.search) {
						this.extractPage(nextProps.location.search);
				}
		}

		render() {
				let posts = window.posts.slice(10 * (this.state.page - 1), 10 * this.state.page)
				document.title = window.config.title;

				if (posts === null || posts === undefined) {
						return <Loader/>
				}
				let items = posts.map((post, ind) => <section className="post" key={ind /*post.id*/}>
						<header className="post-header">
								<Link to={post.link}>
										<h2 className="post-title">{post.title}</h2>
								</Link>

								<p className="post-meta">
										Posted at {post.date}
								</p>
						</header>

						<div className="post-description">
							<p dangerouslySetInnerHTML={{
								__html: post.excerpt + '...'
							}}/>
						</div>
				</section>)
				return (
						<div className="posts">
								<h1 className="content-subhead">Latest posts</h1>
								{items}
								<button onClick={this.prevPage}>Prev page</button>
								<button onClick={this.nextPage}>Next page</button>
						</div>
				);
		}
}

export default PostList;