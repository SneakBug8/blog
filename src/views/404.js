import React, { Component } from 'react';

class Error404 extends Component {
    constructor (props) {
        super()

        document.title = "Error 404";
    }
    render() {
        return (
            <div>
            <h3>404 page not found</h3>
            <p>We are sorry but the page you are looking for does not exist.</p>
          </div>);
    }
}

export default Error404;