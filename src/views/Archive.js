import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import Loader from './../components/Loader.js';
import Page from './Page';

/* Single Post page*/
class Archive extends Component {

    constructor (props) {
        super()

        this.posts = window.posts;

        document.title = window.config.title + window.config.separator + "Archive";        
    }
    render() {
        let posts = this.posts;
        if (posts === null || posts === undefined) {
            return <Loader/>
        }
        let res = posts.map((post) =>
            <li key={post.id}><Link to={post.link}>{post.title}</Link></li>);
        return (
        <Page title="Archive" meta="All posts on this site">
            <ul>
                {res}
            </ul>
        </Page>)
        }
}

export default Archive;