import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import Loader from './../components/Loader.js';

/* Single Post page*/
class Page extends Component {

    constructor (props) {
        super()  
    }
    render() {
        return (
            <section class="post">
            <header class="post-header">
                <h2 class="post-title">{this.props.title}</h2>

                <p class="post-meta">
                    {this.props.meta}
                </p>
            </header>
            <div class="post-description">
                {this.props.children}
            </div>
        </section>
        );
    }
}

export default Page;